<?php
//DEV
//error_reporting(E_ALL ^ E_NOTICE);
//ini_set('display_errors', 1);

//PROD
header('Content-Type: application/json');
error_reporting(0);
ini_set('display_errors', 0);


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'application/phpmailer/src/Exception.php';
require 'application/phpmailer/src/PHPMailer.php';
require 'application/phpmailer/src/SMTP.php';
require 'application/helper.php';

//RESULT
$result['result'] = 2;
$result['text'] = null;


//FILTROWANIE
$_POST['name'] = filtrujTytul($_POST['name']);
$_POST['email'] = filtrujTytul($_POST['email']);
$_POST['phone'] = filtrujTytul($_POST['phone']);
$_POST['comment'] = nl2br(filtrujTytul($_POST['comment']));



//SPRAWDZENIE WYPELNIENIA
if (checkCompletition()) {

  $subject = 'BinaryFrontier: Contact form - general contact';
  $body = 'NAME: '.$_POST['name'];
  $body .= '<br>E-MAIL: '.$_POST['email'];
  $body .= '<br>PHONE: '.$_POST['phone'];
  $body .= '<br><br>COMMENT:<br>'.$_POST['comment'];

  $mail = new PHPMailer(true);
  try {

    //server
    $mail->SMTPDebug = 0;
    $mail->isSMTP();
    $mail->CharSet = "utf-8";
    $mail->Host = 'mail.binaryfrontier.com';
    $mail->SMTPAuth = true;
    $mail->Username = '0xbekin@binaryfrontier.com';
    $mail->Password = 'ZNL@mgL$z2c$';
    $mail->SMTPSecure = 'ssl';
    $mail->Port = 465;

    //odbiorcy
    $mail->setFrom('0xbekin@binaryfrontier.com');
    $mail->addAddress('0xbekin@binaryfrontier.com');
    $mail->addReplyTo($_POST['email'], $_POST['name']);

    //content
    $mail->isHTML(true);
    $mail->Subject = $subject;
    $mail->Body    = $body;

    $mail->send();
    $result['result'] = 1;
    $result['text'] = 'Message sent.';
  } catch (Exception $e) {
    $result['result'] = 2;
    $result['text'] = 'Server error. Please try again or contact us by e-mail: 0xbekin@binaryfrontier.com';
  }
} else {
  $result['result'] = 2;
  $result['text'] = 'You must fill at least Name, E-mail, Phone and Comment.';
}
echo json_encode($result);
?>
