import 'babel-polyfill';
import $ from 'jquery';
import menu from './components/menu/menu';
import contactForm from './components/contactForm/contactForm';
import mainContent from './components/mainContent/mainContent';
import popupContact from './components/popupContact/popupContact';
import mediumPage from './components/mediumPage/mediumPage';
import news from './components/news/news';

$(document).ready(() => {
  menu.init();
  contactForm.init();
  mainContent.init();
  popupContact.init();
  mediumPage.init();
  news.init();
});
