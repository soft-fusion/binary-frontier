import $ from 'jquery';

const news = {
  settings: {
    target: '.news.-black',
    postContainer: '.news__container'
  },
  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.getPosts();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      postContainer: target.find(settings.postContainer)
    };
  },
  truncate(str, no_words) {
    return str
      .split(' ')
      .splice(0, no_words)
      .join(' ');
  },
  getPosts() {
    Promise.all([
      fetch(
        'https://api.rss2json.com/v1/api.json?rss_url=https://blog.binaryfrontier.com/feed' +
          '&nocache=' +
          new Date().getTime()
      ).then(res => res.json()),
      fetch(
        'https://api.rss2json.com/v1/api.json?rss_url=https://medium.com/feed/cryptomanufaktur' +
          '&nocache=' +
          new Date().getTime()
      ).then(res => res.json())
    ]).then(data => {
      let items = data[0].items.concat(data[1].items);
      items.sort((a, b) => new Date(b.pubDate) - new Date(a.pubDate));
      if (
        data[0].status == 'ok' &&
        data[1].status == 'ok' &&
        data[0].items.length > 0 &&
        data[1].items.length > 0
      ) {
        let regex = /(<([^>]+)>)/gi;
        let postTop = document.createElement('div');
        postTop.innerHTML = `
                <div class="news__tag">Medium</div>
                <div class="news__title">Latest News</div>
                <div class="news__grid"> </div>
              `;
        this.$target.postContainer.append(postTop);
        for (let i = 0; i < 3; i++) {
          if (items[i]) {
            let postItem = document.createElement('div');
            $(postItem).addClass('news__item');
            postItem.innerHTML = `
                  <img class="news__itemImage" src="${
                    items[i].thumbnail
                  }" alt="${items[i].title}"/>
                  <div class="news__itemTitle"> ${items[i].title}</div> 
                  <div class="news__itemText"> ${this.truncate(
                    items[i].description.replace(regex, ''),
                    20
                  )}... </div>
                  <a class="news__itemLink" href="${
                    items[i].link
                  }" title="Read more">Read more </a>
              `;
            this.$target.postContainer.find('.news__grid').append(postItem);
          }
        }
      }
    });
  }
};
export default news;
