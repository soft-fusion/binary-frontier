import $ from 'jquery';

const menu = {
  settings: {
    target: '.menu',
    open: '.menu__burger',
    close: '.menu__close',
    box: '.menu__box',
    menuParent: '.menu__link.-parent'
  },
  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
    $(window).on('resize', () => {
      if ($(window).width() > 969) {
        $('.menu__item.-hover').removeClass('-hover');
      }
    });
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      open: target.find(settings.open),
      close: target.find(settings.close),
      box: target.find(settings.box),
      menuParent: target.find(settings.menuParent)
    };
  },
  bindEvents() {
    this.$target.open.on('click', this.openBurger.bind(this));
    this.$target.close.on('click', this.closeBurger.bind(this));
    this.$target.menuParent.on('click', this.openMobileSubMenu.bind(this));
  },
  openBurger() {
    $(this.$target.box).addClass('-active');
    document.body.style.overflow = 'hidden';
    document.body.style.height = '100%';
  },
  closeBurger() {
    $(this.$target.box).removeClass('-active');
    document.body.style.overflow = 'auto';
    document.body.style.height = 'auto';
  },
  openMobileSubMenu(e) {
    if ($(window).width() <= 969) {
      if ($($(e.currentTarget).parent('.menu__item')).hasClass('-hover')) {
        $($(e.currentTarget).parent('.menu__item')).removeClass('-hover');
      } else {
        $($(e.currentTarget).parent('.menu__item')).addClass('-hover');
      }
    }
  }
};
export default menu;
