import $ from 'jquery';
import moment from 'moment';

const mediumPage = {
  settings: {
    target: '.mediumPage',
    topPostContainer: '.topPost',
    postContainer: '.news__container'
  },
  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
      this.getPosts();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      topPostContainer: target.find(settings.topPostContainer),
      postContainer: target.find(settings.postContainer)
    };
  },
  bindEvents() {},
  truncate(str, no_words) {
    return str
      .split(' ')
      .splice(0, no_words)
      .join(' ');
  },
  getPosts() {
    Promise.all([
      fetch(
        'https://api.rss2json.com/v1/api.json?rss_url=https://blog.binaryfrontier.com/feed' +
          '&nocache=' +
          new Date().getTime()
      ).then(res => res.json()),
      fetch(
        'https://api.rss2json.com/v1/api.json?rss_url=https://medium.com/feed/cryptomanufaktur' +
          '&nocache=' +
          new Date().getTime()
      ).then(res => res.json())
    ]).then(data => {
      $.each(data, (index, dat) => {
        $.each(dat.items, (index2, item) => {
          data[index].items[index2].parentUrl = dat.feed.link;
        });
      });
      let items = data[0].items.concat(data[1].items);
      items.sort((a, b) => new Date(b.pubDate) - new Date(a.pubDate));
      if (data[0].status == 'ok' && data[1].status == 'ok') {
        let regex = /(<([^>]+)>)/gi;
        if (items.length > 0) {
          let topPost = document.createElement('div');
          $(topPost).addClass('topPost__container');
          topPost.innerHTML = `
              <div class="topPost__date"> ${moment(items[0].pubDate).format(
                'MMM DD, YYYY'
              )} </div>
              <div class="topPost__title"> ${items[0].title}</div> 
                <img src="${items[0].thumbnail}" alt="${items[0].title}"/>
            <div class="topPost__box">
            <div>
                <div class="topPost__text"> ${this.truncate(
                  items[0].description.replace(regex, ''),
                  20
                )}... </div>
                <a class="topPost__linkMore" href="${
                  items[0].link
                }" title="Read more">Read more </a>
                </div>
                <div class="topPost__buttonPlace">
                  <a class="customButton -blue" href="${
                    items[0].parentUrl
                  }" title="MEDIUM"> MEDIUM </a>
                </div>
                </div>
              </div>`;
          this.$target.topPostContainer.append(topPost);
          if (items.length > 1) {
            let postTop = document.createElement('div');
            postTop.innerHTML = `
                <div class="news__tag">News</div>
                <div class="news__title">Older Posts</div>
                <div class="news__grid"> </div>
              `;
            this.$target.postContainer.append(postTop);
            for (let i = 1; i < 4; i++) {
              if (items[i]) {
                let postItem = document.createElement('div');
                $(postItem).addClass('news__item');
                postItem.innerHTML = `
                  <img class="news__itemImage" src="${
                    items[i].thumbnail
                  }" alt="${items[i].title}"/>
                  <div class="news__itemTitle"> ${items[i].title}</div> 
                  <div class="news__itemText"> ${this.truncate(
                    items[i].description.replace(regex, ''),
                    20
                  )}... </div>
                  <a class="news__itemLink" href="${
                    items[i].link
                  }" title="Read more">Read more </a>
              `;
                this.$target.postContainer.find('.news__grid').append(postItem);
              }
            }
            let link = document.createElement('a');
            link.href = 'https://blog.binaryfrontier.com/';
            link.title = 'Show more';
            link.text = 'Show more';
            $(link).addClass('customButton -blue');
            this.$target.postContainer.append(link);
          }
        } else {
          let topPost = document.createElement('div');
          $(topPost).addClass('topPost__container');
          topPost.innerHTML = `
              <div class="topPost__noPosts">
              There are no posts currently, but you can visit our profile on <a href="https://blog.binaryfrontier.com" title="MEDIUM" class="customButton -blue">Medium </a>
              </div>`;
          this.$target.topPostContainer.append(topPost);
        }
      } else {
        let errorPost = document.createElement('div');
        $(errorPost).addClass('topPost__container');
        errorPost.innerHTML = `
              <div class="topPost__noPosts">
              There are some problems currently, but you can visit our profile on <a href="https://blog.binaryfrontier.com" title="MEDIUM" class="customButton -blue">Medium </a>
              </div>`;
        this.$target.topPostContainer.append(errorPost);
      }
    });
  }
};
export default mediumPage;
