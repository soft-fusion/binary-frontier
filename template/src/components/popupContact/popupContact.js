import $ from 'jquery';

const popupContact = {
  settings: {
    target: '.popupContact',
    close: '.popupContact__close',
    open: '.-openContact'
  },
  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
    setTimeout(() => {
      $(this.$target.root).css('display', 'block'), 400;
    });
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      open: $(settings.open),
      close: target.find(settings.close)
    };
  },
  bindEvents() {
    this.$target.open.on('click', this.openPopup.bind(this));
    this.$target.close.on('click', this.closePopup.bind(this));
  },
  openPopup() {
    $(this.$target.root).addClass('-open');
    document.body.style.overflow = 'hidden';
    document.body.style.height = '100vh';
  },
  closePopup() {
    $(this.$target.root).removeClass('-open');
    document.body.style.overflow = 'auto';
    document.body.style.height = 'auto';
  }
};
export default popupContact;
