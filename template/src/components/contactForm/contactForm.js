import $ from 'jquery';

const contactForm = {
  settings: {
    target: '.contactForm',
    submit: '#button_send',
    name: '#name',
    email: '#email',
    phone: '#phone-number',
    comment: '#comment',
    notification: '.contactForm__notification'
  },
  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      submit: target.find(settings.submit),
      name: target.find(settings.name),
      email: target.find(settings.email),
      phone: target.find(settings.phone),
      comment: target.find(settings.comment),
      notification: target.find(settings.notification)
    };
  },
  bindEvents() {
    this.$target.submit.on('click', this.sendForm.bind(this));
    $(this.$target.phone).on('keydown paste', this.allowPhone.bind(this));
  },
  allowPhone(e) {
    const regex = /^[0-9]+$/;
    if (e.type === 'keydown') {
      if (
        !regex.test(e.key) &&
        e.key !== 'Tab' &&
        e.key !== 'Backspace' &&
        e.key !== 'Enter' &&
        e.key !== 'Control'
      ) {
        e.preventDefault();
      }
    } else if (e.type === 'paste') {
      if (
        !regex.test(
          (
            e.clipboardData ||
            e.originalEvent.clipboardData ||
            window.clipboardData
          ).getData('text')
        )
      ) {
        e.preventDefault();
      }
    }
  },
  sendForm(e) {
    e.preventDefault();
    const name = $(this.$target.name).val();
    const email = $(this.$target.email).val();
    const phone = $(this.$target.phone).val();
    const comment = $(this.$target.comment).val();
    $.ajax({
      url: 'ajax_contact_form.php',
      method: 'POST',
      dataType: 'json',
      data: {
        name: name,
        email: email,
        comment: comment,
        phone: phone
      },
      success: response => {
        if (response.result === 1) {
          $(this.$target.notification).addClass('-success');
          $('.contactForm .customInput').val('');
        } else if (response.result === 2) {
          $(this.$target.notification).addClass('-error');
        }
        $(this.$target.notification)
          .html(response.text)
          .show();
        setTimeout(() => {
          $(this.$target.notification).animate(
            {
              opacity: 0
            },
            500,
            () => {
              $(this.$target.notification)
                .hide()
                .css('opacity', 1);
              $(this.$target.notification).removeClass('-error');
              $(this.$target.notification).removeClass('-success');
            }
          );
        }, 5000);
      }
    });
  }
};
export default contactForm;
