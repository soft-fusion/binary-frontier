import $ from 'jquery';
const mainContent = {
  settings: {
    target: '.mainContent',
    scrollButton: '.mainContent__scrollDown',
    sections: '[data-section]'
  },

  init(args) {
    this.settings = $.extend(true, this.settings, args);
    if ($(this.settings.target).length > 0) {
      this.catchDOM(this.settings);
      this.bindEvents();
    }
  },
  catchDOM(settings) {
    const target = $(settings.target);
    this.$target = {
      root: target,
      scrollButton: target.find(settings.scrollButton),
      sections: $(settings.sections)
    };
  },
  bindEvents() {
    $(this.$target.scrollButton).on('click', this.scrollToNext.bind(this));
  },
  scrollToNext(event) {
    let button = event.currentTarget;
    let parent = $(button).parents('[data-section]');
    let indexParent = this.$target.sections.index(parent);
    if (this.$target.sections[indexParent + 1]) {
      let targetOffset = $(this.$target.sections[indexParent + 1]).offset().top;
      $('html,body').animate({ scrollTop: targetOffset - 50 }, 1000);
    }
  }
};
export default mainContent;
