<?php

function filtrujTytul($string)
{
  return htmlspecialchars(addslashes($string));
}

function checkCompletition()
{
  return
    $_POST['name'] != ''
    && $_POST['email'] != ''
    && $_POST['phone'] != ''
    && $_POST['comment'] != '';
}
